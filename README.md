## Pasta /Types
Foi adicionado os arquivos na pasta "src".
Mudei o composer.json para carregar via PS4-R os autoloads dos Namespace Types.
```sh
{
    "require": {
        "moontoast/math": "^1.1"
    },
    "autoload": {
        "psr-4": {
            "Types\\": "src/"
        }
    }
}
```

## EthereumRPCClient.php
Fiz uma alteração na classe limitando o mesmo a falhas e erros quando ocorrem erros e nao retorna a variavel Result no JSON
```sh
        if (is_array(json_decode($result, true))){
            if (isset(json_decode($result, true)['result']))
                return json_decode($result, true)['result'];
            else
            return json_decode($result, true);
        }else{
            return json_decode($result, true);
        }
```

### Instalando

Caso não esteja usando o Docker com a imagem do ethereum-clientgo será necessário instalar o Ethereum e o Geth.
```sh
sudo apt-get install software-properties-common
```
```sh
sudo add-apt-repository -y ppa:ethereum/ethereum
```
```sh
sudo apt-get update
```
```sh
sudo apt-get install ethereum     
```

Deve ser feito os comando abaixos para instaciar uma Private Network para gerar ja 2 contas teste e o mesmo ja iniciar com coins na conta para testar a transferencia de dados.

```sh
cd ~ 
mkdir datadir
mkdir eth-private
```
Troque <UMA_SENHA> para uma senha que serão usadas nas contas (wallets)
```sh
echo <UMA_SENHA> >> password.txt
```
```sh
geth --datadir=./datadir --password password.txt account new
```
<<NUMERO DA CONTA 1>>
```sh
geth --datadir=./datadir --password password.txt account new
```
<<NUMERO DA CONTA 2>>

###<<COPIAR O ARQUIVO GENESIS.JSON E ALTERAR AS LINHAS ABAIXO PELOS NUMEROS DAS CONTAS GERADAS>>
```sh        
   "alloc": {
       "<<NUMERO DA CONTA 1>>": {
           "balance": "0x4000000"
       },
       "<<NUMERO DA CONTA 2>>": {
           "balance": "0"
       }
    }        
```
###Comando para inicializar a carteira e as contas 1 e 2 
        
```sh        
geth --datadir=./datadir init genesis.json
```
###Comando para inicializar o console e a Private Network na porta 8545 da sua rede.
```sh 
geth --identity "base" --rpc --rpcport "8545" --rpccorsdomain "*" --rpcapi "admin,db,eth,debug,miner,net,shh,txpool,personal,web3" --datadir=./datadir --port "30303" --nodiscover console
```
##Teste de Transacao via console
```sh 
personal.unlockAccount("CONTA1")
personal.unlockAccount("CONTA2")

var tx = {from: "<CONTA1>", to: "<CONTA2>", value: web3.toWei(2, "ether")}
personal.sendTransaction(tx, "<PASSOWORD DA CONTA>") 

txpool.content

 pending: {
    0x7fE82b691281F82F274B296075Da63bdC65857cb: {
      9: {nt
.
.
.
miner.start(1)
<<AGUARDAR uns 3min para atualizar o blockchain das transacoes>>
miner.stop()

txpool.content

pending:{}
```
##Programação do Script PHP Index.

###Adicionar no header
```sh 
use \Types\Address as Address;
use \Types\Transaction as Transaction;
use \Types\Ether as Ether;
use \Types\Wei as Wei;
```
Conectar com o Ethereum (lembrar de mudar o currency.ini para a porta 8545)

```sh 
$ethereum = connect_ethereum();
//Imprimi as Contas
foreach ($ethereum->eth_accounts() as $index=>$accounts){
    $balance = new Wei(hexdec($ethereum->eth_getBalance($accounts, "latest")));
    echo "Numero da Conta ". $index. " = ". $accounts." - Saldo ->".$balance->toEther().PHP_EOL;
}
```
//Verifica se tem + de 1 conta na carteira do servidor local.
```sh 
if (count($ethereum->eth_accounts())>1){
    $fromAddress = new Address($ethereum->eth_coinbase());
    $toAddress = new Address($ethereum->eth_accounts()[1]);
    $balance = ($ethereum->eth_getBalance("0xba4f7dea7bffe5f83ad609ef44edebb8198b90b1", "latest"));
    //echo "send transaction<br>";
    $passphrase = "<<SENHA_USADA_NA_CRIACAO_DA_CONTA>>";
    $value= new Ether(10);
    //NUMERO DA TRANSACAO HASH
    $transacao = ($ethereum->personal_sendTransaction($fromAddress->toString(), $toAddress->toString(), $passphrase, $value->amount(), $unit='ether'));    
}
```
###SEMPRE APOS FAZER UMA TRANSACAO RODAR NO CONSOLE DO GETH

Como nao esta em uma rede da ethereum precisa ficar rodando o Miner para atualizar a rede de blockchain e as transacoes sairem do estado de Pending para concluida e assim confirmando a transferencia de dados.
Lembrando que a conta 1 sempre que rodar o miner.start() vai receber mais moedas pois ela é a coinbase (conta base) de mineração.

```sh 
miner.start(1)
<<AGUARDAR UNS 3MINUTOS PARA OS BLOCKCHAINS EFETIVAREM AS TRANSACOES>>
miner.stop()
```
